import java.util.LinkedList;
import java.util.List;

/**
 * Created by amen on 8/10/17.
 */
public enum AsteriskSingletonEnum {
    instance;

    private List<String> calls = new LinkedList<>();

    public void call(){

    }

    public List<String> getCalls(){
        return calls;
    }
}
